package chapter6.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;

@WebFilter ( urlPatterns = {"/edit", "/setting" })

public class LoginFilter implements Filter{

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		 // request,responseの型変換  servlet request,response → HTTPservlet request,response

		HttpSession session = ((HttpServletRequest) request).getSession();
        User loginUser = (User) session.getAttribute("loginUser");
		if(loginUser == null){
			List<String> errorMessages = new ArrayList<String>();
		    errorMessages.add("ログインしてください");
		    session.setAttribute("errorMessages", errorMessages);
		    HttpServletResponse httpResponse = (HttpServletResponse) response;
		    httpResponse.sendRedirect("./login");
		    return;
		}

		chain.doFilter(request, response); // サーブレットを実行

	}

	@Override
	public void destroy() {

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {

	}
}