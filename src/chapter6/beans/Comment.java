package chapter6.beans;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {

    private int id;
    private int userId;
    private String text;
    private Date createdDate;
    private Date updatedDate;
    private int messageId;

    // id: getter/setter method
    public int getId() {
    	return id;
    }

    public void setId(int id) {
    	this.id = id;
    }

    // userId: getter/setter method
    public int getUserId() {
    	return userId;
    }

    public void setUserId(int userId) {
    	this.userId = userId;
    }
    // text: getter/setter method
    public String getText() {
    	return text;
    }

    public void setText(String text) {
    	this.text = text;
    }

    // messageId: getter/setter method
    public int getMessageId() {
    	return messageId;
    }

    public void setMessageId(int messageId) {
    	this.messageId = messageId;
    }
    // createdDate: getter/setter method
    public Date getCreatedDate() {
    	return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
    	this.createdDate = createdDate;
    }
    // updatedDate: getter/setter method
    public Date getUpdatedDate() {
    	return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
    	this.updatedDate = updatedDate;
    }
    // getter/setterは省略されているので、自分で記述しましょう。
}