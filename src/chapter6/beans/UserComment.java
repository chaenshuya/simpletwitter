package chapter6.beans;

import java.io.Serializable;
import java.util.Date;

public class UserComment implements Serializable {

    private int id;
    private String account;
    private String name;
    private int userId;
    private int messageId;
    private String text;
    private Date createdDate;

    // id: getter/setter method
    public int getId() {
    	return id;
    }

    public void setId(int id) {
    	this.id = id;
    }
	// account: getter/setter method
	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	// name: getter/setter method
	public String getName() {
		return name;
	}

	public void setName(String name){
    	this.name = name;
	}

    // userId: getter/setter method
    public int getUserId() {
    	return userId;
    }

    public void setUserId(int userId) {
    	this.userId = userId;
    }
    // messageId: getter/setter method
    public int getMessageId() {
    	return messageId;
    }

    public void setMessageId(int messageId) {
    	this.messageId = messageId;
    }

    // text: getter/setter method
    public String getText() {
    	return text;
    }

    public void setText(String text) {
    	this.text = text;
    }

    // createdDate: getter/setter method
    public Date getCreatedDate() {
    	return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
    	this.createdDate = createdDate;
    }

    // getter/setterは省略されているので、自分で記述しましょう。
}