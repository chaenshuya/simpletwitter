package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		String protoId = request.getParameter("id");
		int messageId;
		Message message = null;

		if (!StringUtils.isBlank(protoId) && protoId.matches("^[0-9]+$")) {
			messageId = Integer.parseInt(protoId);
			message = new MessageService().select(messageId);
		}

		if (message == null) {
			session.setAttribute("errorMessages", "不正なパラメータが入力されました");
			response.sendRedirect("./");
			return;
		}

		request.setAttribute("message", message);

		RequestDispatcher rd = request.getRequestDispatcher("edit.jsp");
		rd.forward(request, response);
	}

	// ここにedit.jspからmessage.text(変更後)を受け取ってmessage.idと引数にしMessageServiceのeditメソッドを呼び出し、TopservletにリダイレクトするdoPostメソッドを追加
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();

		Message message = new Message();
		message.setText(request.getParameter("text"));
		message.setId(Integer.parseInt(request.getParameter("id")));

		if (!isValid(message.getText(), errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("message", message);

			RequestDispatcher rd = request.getRequestDispatcher("edit.jsp");
			rd.forward(request, response);

			return;
		}

		new MessageService().update(message);
		response.sendRedirect("./");
	}

	private boolean isValid(String text, List<String> errorMessages) {

		if (StringUtils.isBlank(text)) {
			errorMessages.add("メッセージを入力してください");
		} else if (140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}