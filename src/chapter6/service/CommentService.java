package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter6.beans.Comment;
import chapter6.beans.UserComment;
import chapter6.dao.CommentDao;
import chapter6.dao.UserCommentDao;

public class CommentService {

	public void insert(Comment comment) {

        Connection connection = null;
        try {
            connection = getConnection();
            new CommentDao().insert(connection, comment);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
	 /*
	 * selectの引数にString型のuserIdを追加
	 */
    public List<UserComment> select() {
        final int LIMIT_NUM = 1000;

        Connection connection = null;
        try {
          connection = getConnection();
          /*
          /*
          * commentDao.selectに引数としてInteger型のidを追加
          * idがnullだったら全件取得する
          * idがnull以外だったら、その値に対応するユーザーIDの投稿を取得する
          */
          List<UserComment> comments = new UserCommentDao().select(connection, LIMIT_NUM);
          commit(connection);

          return comments;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}
